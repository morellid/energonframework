 
#include <stdlib.h>
#include <stdio.h>
  

void square_dgemm (int n, double *A, double *B, double *C)
{
  // For each row i of A 
  int i=0;
  int j=0;
  int k=0;
  double cij;
  for(i = 0;i<n;++i)
  {
    // For each column j of B 
    for(j = 0; j < n; ++j) 
    {
      // Compute C(i,j)
      cij = C[i+j*n];
      for(k = 0; k < n; k++ )
		cij += A[i+k*n] * B[k+j*n];
      C[i+j*n] = cij;
    }
  }
  
}

 
//#define ARRAY_SIZE 250000
 
//int my_array[ARRAY_SIZE];
unsigned int *my_array;
double *_A;
double *_B;
double *_C;

 
unsigned int fill_array(int array_size, double *A)
{
  int indx;
  unsigned int checksum = 0; 
  for (indx=0; indx < array_size; ++indx)
  {
    checksum += A[indx] = rand() % array_size;
  }
  return checksum;
}


int main(int argc, char *argv[])
{
	int indx;
	int array_size;
	if (argc > 1)
	{
		array_size = atoi(argv[1]);
	}
	else
	{
		array_size = 256;
	}

	_A = (double *) malloc(sizeof(double)*array_size*array_size);
	_B = (double *) malloc(sizeof(double)*array_size*array_size);
	_C = (double *) malloc(sizeof(double)*array_size*array_size);
	fill_array(array_size*array_size, _A);
	fill_array(array_size*array_size, _B);
 
	square_dgemm(array_size, _A, _B, _C);

	free(_A);
	free(_B);
	free(_C);
	return(0);
}
 
 
